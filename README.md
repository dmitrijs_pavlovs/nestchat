## Description

[Nest](https://github.com/nestjs/nest) Nestjs simple websocket chat app example.

## Installation

```bash
$ npm install
```

## Running the app

Run

```bash
$ npm run start
```

and navigate to http://localhost:7800/index.html
